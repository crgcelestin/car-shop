import React from 'react';
import { NavLink } from 'react-router-dom'
class CreateTech extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: "",
            employee_number: "",
            hasTechCreated: false,
        };
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state }
        delete data.hasTechCreated
        const TechsUrl = 'http://localhost:8080/api/techs/'
        const datajson = JSON.stringify(data)
        const fetchConfig = {
            method: 'post',
            body: datajson,
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(TechsUrl, fetchConfig)
        if (response.ok) {
            const newTech = await response.json()
            const cleared = {
                name: '',
                employee_number: '',
                hasTechCreated: true,
            };
            this.setState(cleared)
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        let successAlert = "alert alert-success d-none mb-0"
        let formClass = '';
        if (this.state.hasTechCreated) {
            successAlert = "alert alert-success mb-0"
            formClass = 'd-none';
        }
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create A New Technician</h1>
                        <form
                            className={formClass}
                            onSubmit={this.handleSubmit}
                            id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Name"
                                    required type="text"
                                    id="name"
                                    name="name"
                                    className="form-control"
                                    value={this.state.name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input
                                    onChange={this.handleInputChange}
                                    placeholder="Employee Number"
                                    required type="text"
                                    name="employee_number"
                                    id="employee_number"
                                    className="form-control"
                                    value={this.state.employee_number} />
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                        <div
                            className={successAlert}
                            id="success-message">
                            Congratulations! You Created a Technician!
                            <div>
                                <button className="btn-block">
                                    <NavLink className="nav-link" to="/techs/list/">Go To List of Technicians</NavLink>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default CreateTech;
