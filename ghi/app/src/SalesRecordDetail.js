import React from 'react';

class SalesRecordDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales_person_name: '',
            sales_records: []
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleOnClick = this.handleOnClick.bind(this);
    }

    async handleOnClick(event) {
        const SalesUrl = `http://localhost:8090/api/sales/${event}/`;
        const salesResponse = await fetch(SalesUrl);
        if (salesResponse.ok) {
            const data = await salesResponse.json();
            console.log(data)
            this.setState({ sales_records: data.sales_records });
        } else {
            alert('Invalid search, please try a valid sales tech name');
        }
    }

    handleInputChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        return (
            <>
                <p></p>
                <div className="input-group mb-3">
                    <input
                        onChange={this.handleInputChange}
                        type="text"
                        id="sales_person_name"
                        name="sales_person_name"
                        className="form-control"
                        placeholder="Sales Person Name"
                        aria-describedby="basic-addon2"
                        value={this.state.sales_person_name}
                    />
                    <div className="input-group-append">
                        <button
                            className="btn btn-outline-secondary"
                            type="button"
                            onClick={() => this.handleOnClick(this.state.sales_person_name)}
                        >
                            Search Sales Person
                        </button>
                    </div>
                </div>
                <h1> Sales Records</h1>
                <div className="container-fluid p-4 my-1">
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">VIN</th>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Price</th>
                                <th scope="col">Sales Person</th>
                            </tr>
                        </thead>

                        <tbody>
                            {this.state.sales_records.map((record) => (
                                <tr key={record.id}>
                                    <td>{record.automobile.vin}</td>
                                    <td>{record.customer.name}</td>
                                    <td>{record.price}</td>
                                    <td>{record.sales_person.name}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </>
        );
    }
}

export default SalesRecordDetail;
