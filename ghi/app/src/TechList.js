import React from 'react';
class ListTechs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            techs: [],
        };
    }
    async componentDidMount() {
        const TechUrl = "http://localhost:8080/api/techs/";
        const TResponse = await fetch(TechUrl)
        if (TResponse.ok) {
            const data = await TResponse.json()
            this.setState({ 'techs': data.techs })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <h1> Technicians </h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Employee Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.techs.map((tech) => {
                            return (
                                <tr key={tech.id}>
                                    <td>{tech.name}</td>
                                    <td>{tech.employee_number}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ListTechs;
