import React from 'react'
import {NavLink} from 'react-router-dom'

class SalesList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            "sales_records":[]
        }
    }

    async componentDidMount(){
        const recordsUrl = "http://localhost:8090/api/salesrecords/"
        let response = await fetch (recordsUrl)

        if (response.ok){
            let data = await response.json()
            this.setState({'sales_records': data.sales_records})
        }
    }

    render(){
        return (
            <>
            <div className="px-4 py-5 my-5 mt-0 text-center">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" alt="" width="600" />
                    <h1 className="display-5 fw-bold">Sales Records</h1>

                    </div>
            <table className="table table-bordered ">
                <thead>
                    <tr>
                        <th>Sales Employee</th>
                        <th>Customer</th>
                        <th>Vin</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.sales_records.map((record) => {
                        return (
                            <tr className="w-25 p-3" key={record.id}>
                                <td> {record.sales_person.name} </td>
                                <td> {record.customer.name} </td>
                                <td> {record.automobile.vin} </td>
                                <td> {record.price} </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <button><NavLink className="nav-link active" aria-current="page" to="/salesrecords">Add a Sale</NavLink></button>
            </>
        )
    }

}

export default SalesList
