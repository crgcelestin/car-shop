import React from "react";


class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            employee_number: '',
        }

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeEmployeeNumber = this.handleChangeEmployeeNumber.bind(this);
    }

    handleInputChange(event) {
        const target = event.target
        const value = target.value
        const name = target.name
        const employee_number = target.employee_number
        this.setState({
            [name]: value,
            [employee_number]: value,
        });
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };


    const url = 'http://localhost:8090/api/salespeople/'
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers:{
            'Content-Type': 'application/json',
        },
    };
        const response = await fetch (url, fetchConfig);
        if (response.ok){
            const newSalesPerson = await response.json();
            const cleared = {
                name: '',
                employee_number: ''
            }
            this.setState(cleared)
        } else {
            throw new Error('response for new sales person not ok')
        }
    }


    handleChangeName(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleChangeEmployeeNumber(event) {
        const value = event.target.value;
        this.setState({ employee_number: value })
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a new Sales Employee</h1>
                        <form onSubmit={this.handleSubmit} id="create-salesperson-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeName} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeEmployeeNumber} value={this.state.employee_number} placeholder="EmployeeNumber" required type="number" name="employee_number" id="employee_number" className="form-control" />
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>

        );
    }
}

export default SalesPersonForm
