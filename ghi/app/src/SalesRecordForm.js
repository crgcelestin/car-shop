import React from "react";

class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            autos: [],
            sales_people: [],
            customers: [],
            price: '',
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this);
        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
        this.handleChangePrice = this.handleChangePrice.bind(this);

    }


    async componentDidMount() {
        const autourl = 'http://localhost:8090/api/automobiles/';
        const autoresponse = await fetch(autourl);

        const sales_peopleurl = "http://localhost:8090/api/salespeople/";
        const salespeopleresponse = await fetch (sales_peopleurl);

        const customersurl = "http://localhost:8090/api/customers/"
        const customersresponse = await fetch (customersurl)

        if (autoresponse.ok && salespeopleresponse.ok && customersresponse.ok ) {
          const autodata = await autoresponse.json();

          const salespeopledata = await salespeopleresponse.json();
          const customersdata = await customersresponse.json();

          this.setState({automobiles: autodata.automobiles});
          this.setState({sales_people: salespeopledata.sales_person});
          this.setState({customers: customersdata.customers});
        }
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {
            automobile: this.state.automobile,
            sales_person: this.state.sales_person,
            customer: this.state.customer,
            price: this.state.price,
        }

    const url = 'http://localhost:8090/api/salesrecords/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newSalesRecord = await response.json();
        const cleared = {
            automobile:'',
            sales_person:'',
            customer:'',
            price: '',

        }
        this.setState(cleared)
        this.componentDidMount()
        } else {
            throw new Error('response not working')
        }
    }


    handleChangeAutomobile(event) {
        const value = event.target.value;
        this.setState({ automobile: value })
    }

    handleChangeSalesPerson(event) {
        const value = event.target.value;
        this.setState({ sales_person: value })
    }

    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value })
    }

    handleChangePrice(event) {
        const value = event.target.value;
        this.setState({ price: value })
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    <h1>Add a new Sales Record</h1>
                    <form onSubmit={this.handleSubmit} id="create-salesrecord-form">
                        <div className="mb-3">
                            <select onChange={this.handleChangeAutomobile} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                            <option value="">Choose an Automobile</option>
                            {this.state?.automobiles?.map(automobile => {
                                return (
                                <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChangeSalesPerson} value={this.state.sales_person} required name="sales_person" id="sales_person" className="form-select">
                            <option value="">Choose a Sales Employee</option>
                            {this.state.sales_people?.map(sales_person => {
                                return (
                                <option key={sales_person.id} value={sales_person.id}>{sales_person.name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={this.handleChangeCustomer} value={this.state.customer} required name="customer" id="customer" className="form-select">
                            <option value="">Choose a Customer</option>
                            {this.state?.customers?.map(customer => {
                                return (
                                <option key={customer.id} value={customer.id}>{customer.name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={this.handleChangePrice} value={this.state.price} placeholder="price" required type="number" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Add Sale</button>
                    </form>
                    </div>
                </div>
            </div>

        );
    }

}

export default SalesRecordForm
