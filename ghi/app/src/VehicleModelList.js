import React from 'react';
import { NavLink } from 'react-router-dom';

class VehicleList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            models: []
        };
        this.handleOnClick = this.handleOnClick.bind(this)
    }
    async handleOnClick(id) {
        const VmsUrl = `http://localhost:8100/api/models/${id}/`
        const fetchConfig = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(VmsUrl, fetchConfig)
        if (response.ok) {
            const response2 = await fetch(`http://localhost:8100/api/models/`)
            if (response2.ok) {
                const data = await response2.json()
                this.setState({ 'models': data.models })
            }
        }
    }
    async componentDidMount() {
        const modelsUrl = 'http://localhost:8100/api/models/'
        const modelResponse = await fetch(modelsUrl)
        if (modelResponse.ok) {
            const data = await modelResponse.json()
            this.setState({ 'models': data.models })
        } else {
            throw new Error('response is not ok')
        }
    }
    render() {
        return (
            <div className="container-fluid p-4 my-1">
                <h1> Vehicles</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Manufacturer</th>
                            <th scope="col">Picture</th>
                            <th scope="col">Remove</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.models.map((model) => {
                            return (
                                <tr key={model.id}>
                                    <td>{model.name}</td>
                                    <td>{model.manufacturer.name}</td>
                                    <td>
                                        <img
                                            src={model.picture_url}
                                            width={150}
                                            alt='pic img'
                                        />
                                    </td>
                                    <td>
                                        <button className="btn btn-danger" onClick={() => this.handleOnClick(model.id)}>
                                            DELETE</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default VehicleList;
