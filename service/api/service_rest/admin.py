from django.contrib import admin
from .models import AutoVO, Technician, Appointment
# Register your models here.
admin.site.register(AutoVO)
admin.site.register(Technician)
admin.site.register(Appointment)
