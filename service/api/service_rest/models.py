from django.db import models
from django.urls import reverse

class AutoVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    import_href=models.CharField(max_length=200, unique=True, null=True)
    import_vin=models.CharField(max_length=17, unique=True, null=True)

    def __str__(self):
        return f"{self.import_vin}"


class Technician(models.Model):
    name=models.CharField(max_length=200)
    employee_number=models.PositiveIntegerField(unique=True)
    def get_api_url(self):
        return reverse('api_list_techs', kwargs={"pk":self.pk})
    def __str__(self):
        return f"{self.name}"
    class Meta:
        ordering=("name",)

class Appointment(models.Model):
    vin=models.CharField(max_length=200, null=True)
    date=models.DateTimeField()
    reason=models.TextField()
    owner=models.CharField(max_length=200)
    tech=models.ForeignKey(
        Technician,
        related_name='technician_appointment',
        on_delete=models.PROTECT
    )
    is_finished=models.BooleanField(default=False)
    is_canceled=models.BooleanField(default=False)
    vip=models.BooleanField(default=False)
    def get_api_url(self):
        return reverse('api_show_appointment', kwargs={"pk":self.pk})
    def __str__(self):
        return f"{self.date} +{self.reason}"
