from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import AutomobileVO, SalesPerson, Customer, SalesRecord
import json


class AutomobileVOEncoder (ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href",]


class SalesPersonEncoder (ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class CustomerEncoder (ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
        "id",
    ]


class SalesRecordEncoder (ModelEncoder):
    model = SalesRecord
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "price",
        "id"
    ]
    encoders = {
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
        "automobile": AutomobileVOEncoder()
    }

@require_http_methods(["GET"])
def api_list_automobiles(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.filter(has_sold=False)
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
        )

@require_http_methods(["GET", "POST"])
def api_list_sales_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_people},
            encoder = SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create sales person"},
                status=400,
            )


@require_http_methods(["DELETE","GET","PUT"])
def api_show_sales_people(request,id):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sales_person = SalesPerson.objects.get(id=id)
            sales_person.delete()
            return JsonResponse(
                {"message": "Sales person deleted"}
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            SalesPerson.objects.filter(id=id).update(**content)
            sales_person = SalesPerson.objects.get(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status = 404,
            )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder = CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could not create customer"},
                status=400,
            )


@require_http_methods(["DELETE","GET","PUT"])
def api_show_customer(request,id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                {"message": "Customer Deleted"}
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)
            Customer.objects.filter(id=id).update(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404,
            )

@require_http_methods(['GET'])
def api_show_sales_record(request, name):
    if request.method == "GET":
        # retrieve a queryset that is the resultant of SalesPerson objects that match name paramater and using filter returns salesperson instances that have that specific name
        sales_people = SalesPerson.objects.filter(name=name)
        # intiialize empty array to contain sales records
        sales_records = []

        # iterate through each SalesPesron object in queryset
        for sales_person in sales_people:
            # fetch every SalesRecord instance associated with current salesperson and then use filter to query the SalesRecord based on the sales person field that has fk relationship
            # Extend adds sales records from retrieved queryset to sales record list, to gain all sales records for sales person
            sales_records.extend(SalesRecord.objects.filter(sales_person=sales_person))

        # return json response object that contains all sales record matching this query series

        return JsonResponse(
            {'sales_records': sales_records},
            encoder=SalesRecordEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_sales_records(request, employee_id=None):
    if request.method == "GET":
        if employee_id == None:
            sales_records = SalesRecord.objects.all()
        else:
            sales_records = SalesRecord.objects.filter(sales_person=employee_id)
        return JsonResponse(
            {"sales_records": sales_records},
            encoder=SalesRecordEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile

            employee_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=employee_id)
            content["sales_person"] = sales_person

            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            sales_record = SalesRecord.objects.create(**content)
            automobile.has_sold=True
            automobile.save()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"message": "Could not create a sales record"},
                status=400,
            )

@require_http_methods(["DELETE", "PUT", "GET"])
def api_show_sales_records (request,id):
    if request.method == "GET":
        try:
            sales_record = SalesRecord.objects.get(id=id)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False,
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Sales record doesnt exist"},
                status=404,
            )
    elif request.method == "DELETE":
        try:
            sales_record = SalesRecord.objects.get(id=id)
            sales_record.delete()
            return JsonResponse(
                {"message": "Sales record deleted"}
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Sales record doesnt exist"}
            )
    else:
        try:
            content = json.loads(request.body)
            sales_record = SalesRecord.objects.get(id=id)
            SalesRecord.objects.filter(id=id).update(**content)
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Does not exist"},
                status=404
            )
