from django.contrib import admin
from django.urls import path

from .views import(
    api_list_automobiles,
    api_list_customers,
    api_list_sales_people,
    api_list_sales_records,
    api_show_customer,
    api_show_sales_people,
    api_show_sales_records,
    api_show_sales_record
)


urlpatterns = [
    path("automobiles/", api_list_automobiles, name="api_list_automobiles"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("customers/<int:id>/", api_show_customer, name="api_show_customer"),
    path("salespeople/", api_list_sales_people, name="api_list_sales_people"),
    path("salespeople/<int:id>/", api_show_sales_people, name="api_show_sales_people"),
    path("salesrecords/", api_list_sales_records, name="api_list_sales_records"),
    path("salesrecords/<int:id>/", api_show_sales_records, name="api_show_sales_records"),
    path('sales/<str:name>/', api_show_sales_record,
    name='api_show_sales_record')
]
